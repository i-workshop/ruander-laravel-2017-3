@extends('layouts.master')

@section('page_title','Contact Us')

@section('content_title')
<h1>Contact Us</h1>
@endsection

@section('content')
<!-- Main component for a primary marketing message or call to action -->
<div class="jumbotron">
    @if(Session::has('status'))
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <strong>Success!</strong> {{ Session::pull('status') }}</div>
    @endif
    <p>Get in touch... send us a message!</p>
    @if($errors->any())
    <ul class="alert alert-danger">
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
    @endif
    <p>
    <form method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label for="email">Email address*</label>
            <input type="text" class="form-control" id="email" placeholder="Email" name="email" value="{{ old('email') }}">{{-- old('fieldname') a mező poston küldött előző valueja) --}}</div>
        <div class="form-group">
            <label for="name">Name*</label>
            <input type="text" class="form-control" id="name" placeholder="John Doe" name="name" value="{{ old('email') }}">
        </div>
        <div class="form-group">
            <label for="msg">Message*</label>
            <textarea id="msg" name="msg" class="form-control">{{ old('msg') }}</textarea>
            <p class="help-block">minimum 10 characters</p>
        </div>
        <div class="checkbox">
            <label for="newsletter">
                <input type="checkbox" name="newsletter" id="newsletter"> Newsletter (spam me please)
            </label>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>

</p>
</div>
@endsection