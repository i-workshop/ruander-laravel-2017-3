Üdv {{ $maildata->name }}

<p>Üzenetet küldött a {{ env('APP_NAME') }} oldalon keresztül.</p>

Üzenet szövege: <br>
{{ $maildata->msg }}
<br><br><br>
{{ env('APP_NAME') }} csapata